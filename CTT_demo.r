#
####### Bootstrapped confirmatory tetrad test #######
#
# This code is a demo script for conducting the bootstrapped confiramatory tetrad 
# tests analysis as presented in the manuscript titled: 
# Direction of dependence between specific symptoms of depression: a non-Gaussian approach.
# García-Velázquez, R.; Jokela, M.; Rosenström, T.H.
# The code is based on the work by Bollen & Ting (1990, 1998, and 2000)

# Regina García-Velázquez / last updated 5th March, 2019.

library(dagitty); library(MVN) 
library(lavaan); library(psych) 
library(Hmisc); library(car)
library(foreign)

download.file("https://wwwn.cdc.gov/Nchs/Nhanes/2005-2006/DPQ_D.XPT", "mentalhealth_2005", mode="wb")
nhanes_dep2005 <- read.xport("mentalhealth_2005")

items <- nhanes_dep2005[,c("dpq010","dpq020","dpq060","dpq090")] 
names(items) <- paste0("x",1:4)
for (i in 1:ncol(items)){
  items[,i] <- car::recode(items[,i], "9=NA", as.numeric=TRUE)
} 

# univariate descriptive info 
describe(items)

# model-implied vanishing tetrads
model <- dagitty("dag{{x1 x2 x3 x4} <- F}")
latents(model) <- c("F")
vanishingTetrads(model, type="within")
# the model implies three vanishing tetrads, of which one is redundant

# check unidimensional model fit
cfa1 <- cfa(model = 'F1 =~ x1 + x2 + x3 + x4',
            std.lv = FALSE, estimator="WLSMV", data= items, ordered = names(items))

# goodness of fit
fitmeasures(cfa1)  

# The following function works only with four itmes 
# and deletes cases if there is any missing value. 
# It does not make any a priori assumption about normality, 
# and is based on the covariance matrix.
# Function coded following Bollen & Ting (1998, 2000)  

tetradtest.bootstrap <- function(data, R=1000){ 
  require(lavaan)

  if (ncol(data)!=4){stop("This function is currently available only for four indicators")}  
  data <- data[complete.cases(data),]
  names(data) <- paste0("i_",1:4); data <- as.matrix(data)  
  N <- nrow(data)

  ######################## 1. sample-based T computation ########################
  
  sample.sigma <- cov(data) # unbiased sample covariance matrix
  ref.cfa <- suppressWarnings(cfa(model = 'F1 =~ i_1 + i_2 + i_3 + i_4',
                                  data = data, std.lv = FALSE,
                                  estimator="DWLS", ordered=names(data)))
  model.implied.sigma <- as.matrix(inspect(ref.cfa, what="sigma.hat"))

  # function calculating the fourth moment, necessary for sigma.ss  
  fourth.moment.estimator <- function(data, row, col, names=NULL){
    N <- nrow(data)
    data <- data[complete.cases(data),]
    if (is.null(names)==TRUE) {
      cov.names <- c("1,2","1,3", "1,4", "2,3", "2,4", "3,4")
      } else {cov.names <- names}
    
    e <- as.numeric(substr(cov.names[row],start = 1,stop = 1))
    f <- as.numeric(substr(cov.names[row],start = 3,stop = 3))
    g <- as.numeric(substr(cov.names[col],start = 1,stop = 1))
    h <- as.numeric(substr(cov.names[col],start = 3,stop = 3))
    
    fourth <- (1/N)*(sum((data[,e]-mean(data[,e]))*(data[,f]-mean(data[,f]))*
                           (data[,g]-mean(data[,g]))*(data[,h]-mean(data[,h]))))
    return(fourth)
  }
  
  # sigma vector with non-redundant covariances contained in the tetrads  
  sigma <- sample.sigma[lower.tri(sample.sigma)]
  names(sigma) <- c("1,2","1,3", "1,4", "2,3", "2,4", "3,4")
  
  t.differences <- matrix(c((sigma[1]*sigma[6] - sigma[2]*sigma[5]), # t1234
                     (sigma[2]*sigma[5] - sigma[3]*sigma[4])), # t1342
                   nrow = 2, ncol=1)
  rownames(t.differences) <- c("t1234","t1342")
  
  
  # covariance matrix of the limiting distribution of the sample covariances in sigma
  # the sample estimator of the fourth moment is non-parametric
  sigma.ss <- matrix(NA, nrow = length(sigma),ncol = length(sigma))
    for(rw in 1:length(sigma)){for (cl in 1:length(sigma)){ 
      sigma.ss[rw,cl] <- fourth.moment.estimator(data=data,
                                                 row=rw, col=cl) - (sigma[rw] * sigma[cl])
    } }

  divis <- matrix(NA, nrow = 6, ncol = 2)
  divis[,1] <- c(sigma[6],-1*sigma[5], 0, 0, -1*sigma[2], sigma[1])
  divis[,2] <- c(0, sigma[5], -1*sigma[4], -1*sigma[3], sigma[2], 0)
  
  sigma.tt <- t(divis) %*% sigma.ss %*% divis  
  # test statistic 
  T.sample <- N %*% t(t.differences) %*% solve(sigma.tt) %*% t.differences  
  
  if(is.null(R)==TRUE || R<1){ 
    list.output <- list("info"= list("bootstraps"= R, "N"=N),
                        "sample.sigma"=round(sample.sigma,3),
                        "model.implied.sigma"= round(model.implied.sigma,3),
                        "sigma.vector" = round(sigma,3),
                        "tetrad.differences" = t.differences,
                        "T.statistic" = data.frame("T"=round(T.sample,3),
                                                   "df"=length(t.differences),
                                                   "p-value"= dchisq(T.sample, df = length(t.differences)))
                        )
    return(list.output)} else{
  ######################## 2. Bootstrap computation ########################  

  # centered data 
  X <- apply(as.matrix(data), 2, scale, center=TRUE, scale=FALSE)[,] 
  # transformed data satisfying null hypothesis
  Z <- X %*% solve(chol(sample.sigma)) %*% chol(model.implied.sigma)
  # initialize vector storing the T distribution 
  T.star <- vector(length = R)

  # bootstrap computations
    for (r in 1:R){
      # * means estimated from bootstrap resample
      # bootstrap samples drawn from Z
      bootX <- Z[sample(nrow(Z), replace=TRUE), ]
      N <- nrow(bootX)
      for(i in 1:ncol(bootX)){bootX[,i] <- as.numeric(bootX[,i])}
      
      S.star <- cov(bootX) # gives unbiased covariance matrix  
      # vector of non-redundant covariances in a bootstrap resample
      sigma.star <-  S.star[lower.tri(S.star)]; names(sigma.star) <- names(sigma)
      
      t.differences.star <- matrix(c((sigma.star[1]*sigma.star[6] - sigma.star[2]*sigma.star[5]), # t1234
                                (sigma.star[2]*sigma.star[5] - sigma.star[3]*sigma.star[4])), # t1342
                              nrow = 2, ncol=1)
      
      sigma.ss.star <- matrix(NA, nrow = length(sigma),ncol = length(sigma))

      for(rw in 1:length(sigma)){for (cl in 1:length(sigma)){ 
        sigma.ss.star[rw,cl] <- fourth.moment.estimator(data=bootX,
                                                        row=rw, col=cl) - ( sigma.star[rw] * sigma.star[cl])
      }
    }
      
 divis.star <- matrix(NA, nrow=6, ncol = 2)
 divis.star[,1] <- c(sigma.star[6],-1*sigma.star[5], 0, 0, -1*sigma.star[2], sigma.star[1])
 divis.star[,2] <- c(0, sigma.star[5], -1*sigma.star[4], -1*sigma.star[3], sigma.star[2], 0)
        
      sigma.tt.star <- t(divis.star) %*% sigma.ss.star %*% divis.star    
      T.star[r] <- (N * t(t.differences.star)) %*% solve(sigma.tt.star) %*% t.differences.star
    } # finish bootstrap
    
  # bootstrap p-value
  bt.result <- sum(as.numeric(T.sample) <= T.star)/R
  
  list.output <- list(
    "info" = list("bootstraps"= R, "N"=N),
    "sample.sigma" = round(sample.sigma,3),
    "model.implied.sigma" = round(model.implied.sigma,3),
    "tetrad.differences" = t.differences,
    "sample.T.statistic" = data.frame("T"=round(T.sample,3),"df"=length(t.differences),
                                      "p-value"= dchisq(T.sample, df = length(t.differences))),
    "bootstrapped.T.vector"= round(T.star,3),
    "bootstrap test"= bt.result
  )
  return(list.output)}
}

# Next line produces the output for the NHANES cycle 2005-2006
tetradtest.bootstrap(items, R = 2000)

