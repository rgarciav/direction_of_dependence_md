## Direction_of_dependence

This repository includes demo material and functions for conducting the analyses included in the paper "Direction of dependence between specific symptoms of depression: a non-Gaussian approach.", by Regina García-Velázquez, Markus Jokela, and Tom Rosenström. Please, cite the latest published version of the paper also when using these scripts or their parts.

The files **Dlingam_notuning.m** and **Dlingamboot_notuning.m** include modified functions that are part of the package DirectLingam 1.2, written by Shimizu and collaborators and found in https://sites.google.com/site/sshimizu06/Dlingamcode. These functions can be used with Octave or Matlab.

The file **demo_script.m** contains a brief code that can be run once downloaded the depression screener-section of the NHANES cohort 2005 (https://wwwn.cdc.gov/nchs/nhanes/search/datapage.aspx?Component=Questionnaire&CycleBeginYear=2005). The variables DPQ010, DPQ020, DPQ060, and DPQ090 shall be selected to reproduce our analyses. This file is written for Octave or Matlab.

The file **CTT_demo.r** is to be used with the same NHANES data and four variables. The code runs a bootstrapped Confirmatory Tetrad Analysis, accordingto the work by Bollen & Ting (1990, 1998, and 2000). This file is written for R.

Thank you for the interest in our work. We hope you find it useful.

Best regards, Regina García Velázquez.