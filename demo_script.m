% First load to your workspace the four variables used in this analysis as an object called 
% NHANESdep2005. If the object was in long shape (size N*4), transpose it so that its size is
% 4*N with the following:
NHANESdep2005 = NHANESdep2005';                             % transpose, the items are rows. 
                                                            % Object type must be "double", not table
                                                            
NHANESdep2005 = NHANESdep2005(:,all(~isnan(NHANESdep2005)));% delete empty cases 

%  function as presented in Shimizu et al (2011)
[B2005, stde2005, ci2005, K2005] = Dlingam(NHANESdep2005); 

%  bootstrapped DirectLiNGAM following Shimizu et al (2011)
[Bsig2005, Asig2005] = Dlingamboot(NHANESdep2005, 20, 0.05); % 20 bootstraps

%  function with no tuning of parameters sigma and kappa at N>=1000
[B2005, stde2005, ci2005, K2005] = Dlingam_notuning(NHANESdep2005); 

%  function with no tuning of parameters sigma and kappa at N>=1000
[Bsig2005, Asig2005] = Dlingamboot_notuning(NHANESdep2005, 20, 0.05); % 20 bootstraps
